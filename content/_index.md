
> *The Lord is my shepherd, I shall not want  
> He makes me down to lie  
> Through pastures green He leadeth me the silent waters by.  
> With bright knives He releaseth my soul.  
> He maketh me to hang on hooks in high places.  
> He converteth me to lamb cutlets,  
> For lo, He hath great power, and great hunger.  
> When cometh the day we lowly ones,  
> Through quiet reflection, and great dedication  
> Master the art of karate,  
> Lo, we shall rise up,  
> And then we'll make the bugger's eyes water.*
>
> --- Sheep's Psalm 23
