+++
title = "nvidia-drivers modeset"
slug = "nvidia-drivers-modeset"
date = 2024-01-24T10:22:11+01:00
tags = ['nvidia', 'modeset', 'kernel']
draft = false
+++

Required by nvidia-prime.

### Enabling modeset

Add to kernel parameters :
```
nvidia-drm.modeset=1
```

Or uncomment this option in
```
# Kernel Mode Setting (notably needed for EGLStream/Wayland)
# Enabling may possibly cause issues with SLI and Reverse PRIME.
options nvidia-drm modeset=1
```

### Checking that modeset is active
```shell
# cat /sys/module/nvidia_drm/parameters/modeset
Y
```

### Links

1. https://wiki.archlinux.org/title/Kernel_parameters
2. https://wiki.gentoo.org/wiki/Hybrid_graphics
3. https://wiki.archlinux.org/title/PRIME
4. https://forums.developer.nvidia.com/t/understanding-nvidia-drm-modeset-1-nvidia-linux-driver-modesetting/204068/3
