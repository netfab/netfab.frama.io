+++
title = "disable firefox disk cache"
slug = "disable-firefox-disk-cache"
date = 2024-11-29T15:46:05+01:00
tags = ['firefox', 'hdd', 'ssd']
draft = false
+++

#### In firefox :

1. in url bar, type : `about:config`
2. into the filter bar, type `browser.cache`
3. find **browser.cache.disk.enable** and set it to `false`
4. find **browser.cache.memory.enable** and set it to `true`
5. find **browser.cache.memory.capacity** and set it to the wanted value.

#### About memory.capacity :

This is the number of kilobytes you want to assign to the cache, or -1 for automatic handling.
See links below.

### Links

1. https://www.arcolinuxforum.com/viewtopic.php?t=146
2. https://kb.mozillazine.org/Browser.cache.disk.enable
3. https://kb.mozillazine.org/Browser.cache.memory.capacity
