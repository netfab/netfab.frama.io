+++
title = "terminal frozen after pressing Ctrl-s shortcut"
slug = "terminal-frozen"
date = 2023-10-24T13:54:01+02:00
tags = ['terminal', 'freeze', 'software-flow-control']
draft = 'false'
+++

### Terminal frozen after pressing *Ctrl-s* shortcut

To unfreeze, press : *Ctrl-q*

### Links

1. https://unix.stackexchange.com/questions/12107/how-to-unfreeze-after-accidentally-pressing-ctrl-s-in-a-terminal
2. https://en.wikipedia.org/wiki/Software_flow_control
