+++
title = "acpid log spam"
slug = "acpid-log-spam"
date = 2023-10-09T16:38:49+02:00
tags = ['acpid', 'syslog']
draft = false
+++

### sys-power/acpid produces log spam

Since many months, **sys-power/acpid** produces a lot of syslog entries when using arrow keys.  
In */var/log/messages* :

> [...]  
> Oct  8 11:15:25 coreI5 root: ACPI event unhandled: button/up UP 00000080 00000000 K  
> Oct  8 11:15:27 coreI5 root: ACPI event unhandled: button/down DOWN 00000080 00000000 K  
> Oct  8 11:17:37 coreI5 root: ACPI event unhandled: button/kpenter KPENTER 00000080 00000000 K  
> [...]  

### Workaround

```shell
$ cat /etc/acpi/events/buttons
```

```shell
# spam in logs : « root: ACPI event unhandled: [...] »
event=button/(up|down|left|right|kpenter)
action=<drop>
```

### Links

1. https://forums.gentoo.org/viewtopic-t-1159391.html
2. https://wiki.archlinux.org/title/acpid#Disabling_ordinary_key_events
