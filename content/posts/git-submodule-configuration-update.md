+++
title = "git: submodule configuration update (new url/branch)"
slug = "git-submodule-configuration-update"
date = 2023-10-24T16:14:24+02:00
tags = ['git', 'hugo']
draft = false
+++

### Updating the configuration of an existing git submodule

Setting new url:
```shell
$ git submodule set-url themes/m10c https://github.com/netfab/hugo-theme-m10c
```

Setting branch (-b for remote branch):
```shell
$ git submodule set-branch -b disable-reading-time themes/m10c
```

Synchronize submodules URL:
```shell
$ git submodule sync --recursive
```

> Synchronisation de l'URL sous-module pour 'themes/m10c'

Updating submodule:
```shell
$ git submodule update --remote
```

> remote: Enumerating objects: 95, done.  
> remote: Counting objects: 100% (95/95), done.  
> remote: Compressing objects: 100% (47/47), done.  
> remote: Total 95 (delta 39), reused 89 (delta 38), pack-reused 0  
> Dépaquetage des objets: 100% (95/95), 18.65 Kio | 3.11 Mio/s, fait.  
> Depuis https://github.com/netfab/hugo-theme-m10c  
>  \* [nouvelle branche] disable-reading-time -> origin/disable-reading-time  
>  Chemin de sous-module 'themes/m10c' : 'bea345fdd7675604f7d5d82e378777377ad58c07' extrait

