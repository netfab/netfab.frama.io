+++
title = "hugo and vim reminder"
slug = "hugo-and-vim-reminder"
date = 2024-01-24T11:29:22+01:00
tags = ['hugo', 'vim']
draft = false
+++

### Local server :

```shell
$ hugo server --ignoreCache --buildDrafts
```

### Vim :
```
:HugoHelperDateIsNow
```

```
:HugoHelperTitleToSlug
```

### Links

1. https://github.com/robertbasic/vim-hugo-helper
